﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OiX
{
    public partial class Form1 : Form
    {
        int won = 0;
        int clicks = 0;
        int level = 0;
        public Form1()
        {
            InitializeComponent();
            NewGame();
            label3.Text = "0";
            label4.Text = "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            check();
            clicks = 0;
        }
        private void check()
        {
            if ((box1.Text == "X") && (box7.Text == "X") && (box4.Text == "X"))
            {
                won = 1;
                box1.BackColor = Color.Red;
                box7.BackColor = Color.Red;
                box4.BackColor = Color.Red;
                winner();
            }
            else if ((box1.Text == "O") && (box7.Text == "O") && (box4.Text == "O"))
            {
                won = 2;
                box1.BackColor = Color.Red;
                box7.BackColor = Color.Red;
                box4.BackColor = Color.Red;
                winner();
            }
            else if ((box2.Text == "X") && (box8.Text == "X") && (box5.Text == "X"))
            {
                won++;
                box2.BackColor = Color.Red;
                box8.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                winner();
            }
            else if ((box2.Text == "O") && (box8.Text == "O") && (box5.Text == "O"))
            {
                won = 2;
                box2.BackColor = Color.Red;
                box8.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                winner();
            }
            else if ((box3.Text == "X") && (box9.Text == "X") && (box6.Text == "X"))
            {
                won++;
                box3.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                box6.BackColor = Color.Red;
                winner();
            }
            else if ((box3.Text == "O") && (box9.Text == "O") && (box6.Text == "O"))
            {
                won = 2;
                box3.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                box6.BackColor = Color.Red;
                winner();
            }
            else if ((box1.Text == "X") && (box2.Text == "X") && (box3.Text == "X"))
            {
                won++;
                box1.BackColor = Color.Red;
                box2.BackColor = Color.Red;
                box3.BackColor = Color.Red;
                winner();
            }
            else if ((box1.Text == "O") && (box2.Text == "O") && (box3.Text == "O"))
            {
                won = 2;
                box1.BackColor = Color.Red;
                box2.BackColor = Color.Red;
                box3.BackColor = Color.Red;
                winner();
            }
            else if ((box4.Text == "X") && (box5.Text == "X") && (box6.Text == "X"))
            {
                won++;
                box4.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box6.BackColor = Color.Red;
                winner();
            }
            else if ((box4.Text == "O") && (box5.Text == "O") && (box6.Text == "O"))
            {
                won = 2;
                box4.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box6.BackColor = Color.Red;
                winner();
            }
            else if ((box7.Text == "X") && (box8.Text == "X") && (box9.Text == "X"))
            {
                won++;
                box7.BackColor = Color.Red;
                box8.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                winner();
            }
            else if ((box7.Text == "O") && (box8.Text == "O") && (box9.Text == "O"))
            {
                won = 2;
                box7.BackColor = Color.Red;
                box8.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                winner();
            }
            else if ((box1.Text == "X") && (box5.Text == "X") && (box9.Text == "X"))
            {
                won++;
                box1.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                winner();
            }
            else if ((box1.Text == "O") && (box5.Text == "O") && (box9.Text == "O"))
            {
                won = 2;
                box1.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box9.BackColor = Color.Red;
                winner();
            }
            else if ((box3.Text == "X") && (box5.Text == "X") && (box7.Text == "X"))
            {
                won++;
                box3.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box7.BackColor = Color.Red;
                winner();
            }
            else if ((box3.Text == "O") && (box5.Text == "O") && (box7.Text == "O"))
            {
                won = 2;
                box3.BackColor = Color.Red;
                box5.BackColor = Color.Red;
                box7.BackColor = Color.Red;
                winner();
            }

            if ((won == 0) && (clicks == 0))
            {
                computermove();
            }
        }
        private void winner()
        {
            if (won == 1)
            {
                console.AppendText("Winner!!!\n");
                score("User");
            }
            else
            {
                console.AppendText("I Won!!!!!!!\n");
                score("computer");
            }
            console.Select(console.Text.Length + 1, 2);
            console.ScrollToCaret();
            box1.Enabled = false;
            box2.Enabled = false;
            box3.Enabled = false;
            box4.Enabled = false;
            box5.Enabled = false;
            box6.Enabled = false;
            box7.Enabled = false;
            box8.Enabled = false;
            box9.Enabled = false;
        }
        private void computermove()
        {

            Random los = new Random();
            int error;
            if (level == 1)
            {
                error = Convert.ToInt32(los.Next(1, 5));
            }
            else if (level == 2)
            {
                error = Convert.ToInt32(los.Next(1, 10));
            }
            else
            {
                error = Convert.ToInt32(los.Next(1, 3));
            }
            if (error != 1)
            {
                if ((box1.Text == "O") && (box3.Text == "O") && (box2.Text == ""))
                {
                    box2.Text = "O";
                }
                else if ((box1.Text == "O") && (box7.Text == "O") && (box4.Text == ""))
                {
                    box4.Text = "O";
                }
                else if ((box7.Text == "O") && (box9.Text == "O") && (box8.Text == ""))
                {
                    box8.Text = "O";
                }
                else if ((box3.Text == "O") && (box9.Text == "O") && (box6.Text == ""))
                {
                    box6.Text = "O";
                }
                /////////////////////////////left site////////////////////////////////////
                else if ((box1.Text == "O") && (box2.Text == "O") && (box3.Text == ""))
                {
                    box3.Text = "O";
                }
                else if ((box4.Text == "O") && (box5.Text == "O") && (box6.Text == ""))
                {
                    box6.Text = "O";
                }
                else if ((box7.Text == "O") && (box8.Text == "O") && (box9.Text == ""))
                {
                    box9.Text = "O";
                }
                ///////////////////////////up site/////////////////////////////////////
                else if ((box7.Text == "O") && (box4.Text == "O") && (box1.Text == ""))
                {
                    box1.Text = "O";
                }
                else if ((box8.Text == "O") && (box5.Text == "O") && (box2.Text == ""))
                {
                    box2.Text = "O";
                }
                else if ((box9.Text == "O") && (box6.Text == "O") && (box3.Text == ""))
                {
                    box3.Text = "O";
                }
                //////////////////////////////right site//////////////////////////////////
                else if ((box2.Text == "O") && (box3.Text == "O") && (box1.Text == ""))
                {
                    box1.Text = "O";
                }
                else if ((box5.Text == "O") && (box6.Text == "O") && (box4.Text == ""))
                {
                    box4.Text = "O";
                }
                else if ((box8.Text == "O") && (box9.Text == "O") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                ///////////////////////////accros/////////////////////////////////////
                else if ((box1.Text == "O") && (box5.Text == "O") && (box9.Text == ""))
                {
                    box9.Text = "O";
                }
                else if ((box5.Text == "X") && (box5.Text == "O") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                /////////////////////////////bottom site/////////////////////////////
                else if ((box1.Text == "O") && (box4.Text == "O") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                else if ((box2.Text == "O") && (box5.Text == "O") && (box8.Text == ""))
                {
                    box8.Text = "O";
                }
                else if ((box3.Text == "O") && (box6.Text == "O") && (box9.Text == ""))
                {
                    box7.Text = "O";
                }
                ///////////////////////////srodki//////////////////////////////////////////
                else if ((box1.Text == "X") && (box3.Text == "X") && (box2.Text == ""))
                {
                    box2.Text = "O";
                }
                else if ((box1.Text == "X") && (box7.Text == "X") && (box4.Text == ""))
                {
                    box4.Text = "O";
                }
                else if ((box7.Text == "X") && (box9.Text == "X") && (box8.Text == ""))
                {
                    box8.Text = "O";
                }
                else if ((box3.Text == "X") && (box9.Text == "X") && (box6.Text == ""))
                {
                    box6.Text = "O";
                }
                /////////////////////////////left site////////////////////////////////////
                else if ((box1.Text == "X") && (box2.Text == "X") && (box3.Text == ""))
                {
                    box3.Text = "O";
                }
                else if ((box4.Text == "X") && (box5.Text == "X") && (box6.Text == ""))
                {
                    box6.Text = "O";
                }
                else if ((box7.Text == "X") && (box8.Text == "X") && (box9.Text == ""))
                {
                    box9.Text = "O";
                }
                ///////////////////////////up site/////////////////////////////////////
                else if ((box7.Text == "X") && (box4.Text == "X") && (box1.Text == ""))
                {
                    box1.Text = "O";
                }
                else if ((box8.Text == "X") && (box5.Text == "X") && (box2.Text == ""))
                {
                    box2.Text = "O";
                }
                else if ((box9.Text == "X") && (box6.Text == "X") && (box3.Text == ""))
                {
                    box3.Text = "O";
                }
                //////////////////////////////right site//////////////////////////////////
                else if ((box2.Text == "X") && (box3.Text == "X") && (box1.Text == ""))
                {
                    box1.Text = "O";
                }
                else if ((box5.Text == "X") && (box6.Text == "X") && (box4.Text == ""))
                {
                    box4.Text = "O";
                }
                else if ((box8.Text == "X") && (box9.Text == "X") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                ///////////////////////////accros/////////////////////////////////////
                else if ((box1.Text == "X") && (box5.Text == "X") && (box9.Text == ""))
                {
                    box9.Text = "O";
                }
                else if ((box5.Text == "X") && (box5.Text == "X") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                /////////////////////////////bottom site/////////////////////////////
                else if ((box1.Text == "X") && (box4.Text == "X") && (box7.Text == ""))
                {
                    box7.Text = "O";
                }
                else if ((box2.Text == "X") && (box5.Text == "X") && (box8.Text == ""))
                {
                    box8.Text = "O";
                }
                else if ((box3.Text == "X") && (box6.Text == "X") && (box9.Text == ""))
                {
                    box9.Text = "O";
                }
                else if (box5.Text == "")
                {
                    box5.Text = "O";
                }
                else if (box1.Text == "")
                {
                    box1.Text = "O";
                }
                else if (box3.Text == "")
                {
                    box3.Text = "O";
                }
                else if (box7.Text == "")
                {
                    box7.Text = "O";
                }
                else if (box9.Text == "")
                {
                    box9.Text = "O";
                }
                else if (box2.Text == "")
                {
                    box2.Text = "O";
                }
                else if (box4.Text == "")
                {
                    box4.Text = "O";
                }
                else if (box6.Text == "")
                {
                    box6.Text = "O";
                }
                else if (box8.Text == "")
                {
                    box8.Text = "O";
                }
                else
                {
                    console.AppendText("Draw!\n");
                    console.Select(console.Text.Length + 1, 2);
                    console.ScrollToCaret();
                    box1.Enabled = false;
                    box2.Enabled = false;
                    box3.Enabled = false;
                    box4.Enabled = false;
                    box5.Enabled = false;
                    box6.Enabled = false;
                    box7.Enabled = false;
                    box8.Enabled = false;
                    box9.Enabled = false;
                }
            }
            else
            {
                if (box5.Text == "")
                {
                    box5.Text = "O";
                }
                else if (box1.Text == "")
                {
                    box1.Text = "O";
                }
                else if (box3.Text == "")
                {
                    box3.Text = "O";
                }
                else if (box7.Text == "")
                {
                    box7.Text = "O";
                }
                else if (box9.Text == "")
                {
                    box9.Text = "O";
                }
                else if (box2.Text == "")
                {
                    box2.Text = "O";
                }
                else if (box4.Text == "")
                {
                    box4.Text = "O";
                }
                else if (box6.Text == "")
                {
                    box6.Text = "O";
                }
                else if (box8.Text == "")
                {
                    box8.Text = "O";
                }
                else
                {
                    console.AppendText("Draw!\n");
                    console.Select(console.Text.Length + 1, 2);
                    console.ScrollToCaret();
                    box1.Enabled = false;
                    box2.Enabled = false;
                    box3.Enabled = false;
                    box4.Enabled = false;
                    box5.Enabled = false;
                    box6.Enabled = false;
                    box7.Enabled = false;
                    box8.Enabled = false;
                    box9.Enabled = false;
                }
            }
            clicks++;
            check();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NewGame();
        }
        private void NewGame()
        {
            won = 0;
            console.AppendText("I am ready to game!\nLet's play!\n");
            console.Select(console.Text.Length + 1, 2);
            console.ScrollToCaret();
            box1.Enabled = true;
            box2.Enabled = true;
            box3.Enabled = true;
            box4.Enabled = true;
            box5.Enabled = true;
            box6.Enabled = true;
            box7.Enabled = true;
            box8.Enabled = true;
            box9.Enabled = true;
            box1.Text = "";
            box2.Text = "";
            box3.Text = "";
            box4.Text = "";
            box5.Text = "";
            box6.Text = "";
            box7.Text = "";
            box8.Text = "";
            box9.Text = "";
            box1.BackColor = Color.WhiteSmoke;
            box2.BackColor = Color.WhiteSmoke;
            box3.BackColor = Color.WhiteSmoke;
            box4.BackColor = Color.WhiteSmoke;
            box5.BackColor = Color.WhiteSmoke;
            box6.BackColor = Color.WhiteSmoke;
            box7.BackColor = Color.WhiteSmoke;
            box8.BackColor = Color.WhiteSmoke;
            box9.BackColor = Color.WhiteSmoke;
            

        }
        public void start_timer()
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            if (box2.Text == "")
            {
                box2.Text = "X";
                clicks = 0;
                check();
            }
            else
            {
            }
        }

        private void box1_Click(object sender, EventArgs e)
        {
            if (box1.Text == "")
            {
                box1.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box3_Click(object sender, EventArgs e)
        {
            if (box3.Text == "")
            {
                box3.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box4_Click(object sender, EventArgs e)
        {
            if (box4.Text == "")
            {
                box4.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box5_Click(object sender, EventArgs e)
        {
            if (box5.Text == "")
            {
                box5.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box6_Click(object sender, EventArgs e)
        {
            if (box6.Text == "")
            {
                box6.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box7_Click(object sender, EventArgs e)
        {
            if (box7.Text == "")
            {
                box7.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box8_Click(object sender, EventArgs e)
        {
            if (box8.Text == "")
            {
                box8.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void box9_Click(object sender, EventArgs e)
        {
            if (box9.Text == "")
            {
                box9.Text = "X";
                clicks = 0;
                check();
            }
            else
            {

            }
        }

        private void easyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            level = 0;
        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            level = 1;
        }

        private void hardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            level = 2;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void score(string who)
        {
            if (who == "User")
            {
                label3.Text = Convert.ToString(Convert.ToInt32(label3.Text) + 1);
            }
            else
            {
                label4.Text = Convert.ToString(Convert.ToInt32(label4.Text) + 1);
            }
            return;
        }
    }
}
