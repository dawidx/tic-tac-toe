﻿namespace OiX
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.console = new System.Windows.Forms.RichTextBox();
            this.box1 = new System.Windows.Forms.Label();
            this.box2 = new System.Windows.Forms.Label();
            this.box3 = new System.Windows.Forms.Label();
            this.box4 = new System.Windows.Forms.Label();
            this.box5 = new System.Windows.Forms.Label();
            this.box7 = new System.Windows.Forms.Label();
            this.box8 = new System.Windows.Forms.Label();
            this.box9 = new System.Windows.Forms.Label();
            this.box6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.levelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.easyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(317, 163);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(246, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Computer Turn";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 373);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(605, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "New Game";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // console
            // 
            this.console.Location = new System.Drawing.Point(12, 205);
            this.console.Name = "console";
            this.console.Size = new System.Drawing.Size(604, 146);
            this.console.TabIndex = 11;
            this.console.Text = "";
            // 
            // box1
            // 
            this.box1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box1.Location = new System.Drawing.Point(47, 35);
            this.box1.Name = "box1";
            this.box1.Size = new System.Drawing.Size(33, 33);
            this.box1.TabIndex = 13;
            this.box1.Text = "X";
            this.box1.Click += new System.EventHandler(this.box1_Click);
            // 
            // box2
            // 
            this.box2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box2.Location = new System.Drawing.Point(107, 35);
            this.box2.Name = "box2";
            this.box2.Size = new System.Drawing.Size(33, 33);
            this.box2.TabIndex = 14;
            this.box2.Text = "X";
            this.box2.Click += new System.EventHandler(this.label3_Click);
            // 
            // box3
            // 
            this.box3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box3.ForeColor = System.Drawing.Color.Black;
            this.box3.Location = new System.Drawing.Point(167, 35);
            this.box3.Name = "box3";
            this.box3.Size = new System.Drawing.Size(33, 33);
            this.box3.TabIndex = 15;
            this.box3.Text = "X";
            this.box3.Click += new System.EventHandler(this.box3_Click);
            // 
            // box4
            // 
            this.box4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box4.Location = new System.Drawing.Point(47, 85);
            this.box4.Name = "box4";
            this.box4.Size = new System.Drawing.Size(33, 33);
            this.box4.TabIndex = 16;
            this.box4.Text = "X";
            this.box4.Click += new System.EventHandler(this.box4_Click);
            // 
            // box5
            // 
            this.box5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box5.Location = new System.Drawing.Point(107, 85);
            this.box5.Name = "box5";
            this.box5.Size = new System.Drawing.Size(33, 33);
            this.box5.TabIndex = 17;
            this.box5.Text = "X";
            this.box5.Click += new System.EventHandler(this.box5_Click);
            // 
            // box7
            // 
            this.box7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box7.Location = new System.Drawing.Point(47, 135);
            this.box7.Name = "box7";
            this.box7.Size = new System.Drawing.Size(33, 33);
            this.box7.TabIndex = 18;
            this.box7.Text = "X";
            this.box7.Click += new System.EventHandler(this.box7_Click);
            // 
            // box8
            // 
            this.box8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box8.Location = new System.Drawing.Point(107, 135);
            this.box8.Name = "box8";
            this.box8.Size = new System.Drawing.Size(33, 33);
            this.box8.TabIndex = 19;
            this.box8.Text = "X";
            this.box8.Click += new System.EventHandler(this.box8_Click);
            // 
            // box9
            // 
            this.box9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box9.Location = new System.Drawing.Point(167, 135);
            this.box9.Name = "box9";
            this.box9.Size = new System.Drawing.Size(33, 33);
            this.box9.TabIndex = 20;
            this.box9.Text = "X";
            this.box9.Click += new System.EventHandler(this.box9_Click);
            // 
            // box6
            // 
            this.box6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.box6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.box6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.box6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box6.Location = new System.Drawing.Point(167, 85);
            this.box6.Name = "box6";
            this.box6.Size = new System.Drawing.Size(33, 33);
            this.box6.TabIndex = 21;
            this.box6.Text = "X";
            this.box6.Click += new System.EventHandler(this.box6_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(621, 24);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.levelToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // levelToolStripMenuItem
            // 
            this.levelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.easyToolStripMenuItem,
            this.mediumToolStripMenuItem,
            this.hardToolStripMenuItem});
            this.levelToolStripMenuItem.Name = "levelToolStripMenuItem";
            this.levelToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.levelToolStripMenuItem.Text = "Level";
            // 
            // easyToolStripMenuItem
            // 
            this.easyToolStripMenuItem.Name = "easyToolStripMenuItem";
            this.easyToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.easyToolStripMenuItem.Text = "Easy";
            this.easyToolStripMenuItem.Click += new System.EventHandler(this.easyToolStripMenuItem_Click);
            // 
            // mediumToolStripMenuItem
            // 
            this.mediumToolStripMenuItem.Name = "mediumToolStripMenuItem";
            this.mediumToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.mediumToolStripMenuItem.Text = "Medium";
            this.mediumToolStripMenuItem.Click += new System.EventHandler(this.mediumToolStripMenuItem_Click);
            // 
            // hardToolStripMenuItem
            // 
            this.hardToolStripMenuItem.Name = "hardToolStripMenuItem";
            this.hardToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.hardToolStripMenuItem.Text = "Hard";
            this.hardToolStripMenuItem.Click += new System.EventHandler(this.hardToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(346, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 126);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Score";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "You";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(133, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Me";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(136, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 3;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(621, 416);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.box6);
            this.Controls.Add(this.box9);
            this.Controls.Add(this.box8);
            this.Controls.Add(this.box7);
            this.Controls.Add(this.box5);
            this.Controls.Add(this.box4);
            this.Controls.Add(this.box3);
            this.Controls.Add(this.box2);
            this.Controls.Add(this.box1);
            this.Controls.Add(this.console);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(637, 452);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(637, 452);
            this.Name = "Form1";
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox console;
        private System.Windows.Forms.Label box1;
        private System.Windows.Forms.Label box2;
        private System.Windows.Forms.Label box3;
        private System.Windows.Forms.Label box4;
        private System.Windows.Forms.Label box5;
        private System.Windows.Forms.Label box7;
        private System.Windows.Forms.Label box8;
        private System.Windows.Forms.Label box9;
        private System.Windows.Forms.Label box6;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem levelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem easyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mediumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hardToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}

